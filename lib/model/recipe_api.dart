import 'dart:convert';
import 'package:foodrecipes/model/recipe.dart';
import 'package:foodrecipes/model/recipe_api.dart';
import 'package:http/http.dart' as http;

class RecipeApi {
// const unirest = require("unirest");

// const req = unirest("GET", "https://yummly2.p.rapidapi.com/feeds/list");

// req.query({
  // "limit": "24",
  // "start": "0"
// });

// req.headers({
  // "X-RapidAPI-Key": "5a42c1154fmsh2f56b303a1847e9p1a87f7jsna7dc207d0f64",
  // "X-RapidAPI-Host": "yummly2.p.rapidapi.com",
  // "useQueryString": true
// });

  static Future<List<Recipe>> getRecipe() async {
    var uri = Uri.https('tasty.p.rapidapi.com', '/recipes/list',
        {"from": "0", "size": "20", "tags": "under_30_minutes"});

    final response = await http.get(uri, headers: {
      "X-RapidAPI-Key": "5a42c1154fmsh2f56b303a1847e9p1a87f7jsna7dc207d0f64",
      "X-RapidAPI-Host": "tasty.p.rapidapi.com",
      "useQueryString": "true"
    });

    Map data = jsonDecode(response.body);

    List _temp = [];

    for (var i in data['results']) {
      _temp.add(i);
    }
    return Recipe.recipeFromSnapshot(_temp);
  }
}
