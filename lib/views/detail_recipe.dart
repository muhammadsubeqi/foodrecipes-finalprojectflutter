import 'package:flutter/material.dart';
import 'package:foodrecipes/views/widget/card_recipe.dart';

class DetailRecipe extends StatelessWidget {
  final String name;
  final String images;
  final String rating;
  final String totalTime;
  final String description;

  DetailRecipe(
      {required this.name,
      required this.images,
      required this.rating,
      required this.totalTime,
      required this.description});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(Icons.restaurant_menu),
            SizedBox(width: 15),
            Text(
              'Detail Food Recipes',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
          ],
        ),
      ),
      body: SafeArea(
          child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          CardRecipe(
              title: name,
              cookTime: totalTime,
              rating: rating.toString(),
              thumbnailUrl: images),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 22, vertical: 10),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Description',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                    textAlign: TextAlign.left,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(description,
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.justify),
                )
              ],
            ),
          )
        ],
      )),
    );
  }
}
