import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:foodrecipes/views/bottom_nav.dart';
import 'package:foodrecipes/views/login.dart';
import 'bottom_nav.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser!.email);
    }
    return Scaffold(
      appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(Icons.home),
              SizedBox(width: 15),
              Text(
                'HOME',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ],
          ),
          actions: [
            IconButton(
              icon: const Icon(
                Icons.favorite,
              ),
              onPressed: () {},
            ),
          ]),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: "Welcome, \n",
                    style: TextStyle(color: Colors.blue[300], fontSize: 20),
                  ),
                  TextSpan(
                    text: auth.currentUser!.email,
                    style: TextStyle(color: Colors.blue[900], fontSize: 20),
                  ),
                ],
              ),
              style: TextStyle(fontSize: 30),
            ),
            // SizedBox(height: 20),
            // TextField(
            //   decoration: InputDecoration(
            //       prefixIcon: Icon(Icons.search, size: 20),
            //       border: OutlineInputBorder(
            //         borderRadius: BorderRadius.circular(20),
            //       ),
            //       hintText: "Search"),
            // ),
            SizedBox(height: 20),
            Container(
              child: Column(
                children: [
                  Image.asset("assets/images/banner.jpg"),
                ],
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("TENTANG | FOODRECIPES",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold))
                ],
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                      "FOOD RECIPES adalah Aplikasi resep makanan. Aplikasi ini bisa membantu anda dalam membuat sebuah menu makananan, di dalam aplikasi ini juga dilengkapi tutorial berupa video yang dapat memudahkan anda dalam mengikuti tutorialnya",
                      style: TextStyle(color: Colors.black, fontSize: 15),
                      textAlign: TextAlign.justify)
                ],
              )),
            ),
            Container(
                child: ElevatedButton(
              onPressed: () {
                _signOut().then((value) => Navigator.of(context)
                    .pushReplacement(
                        MaterialPageRoute(builder: (context) => Login())));
              },
              child: Text('Logout'),
            )),
          ],
        ),
      ),
    );
  }
}
