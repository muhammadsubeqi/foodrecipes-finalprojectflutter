import 'package:flutter/material.dart';
import 'package:foodrecipes/model/recipe.dart';
import 'package:foodrecipes/model/recipe_api.dart';
import 'package:foodrecipes/views/detail_recipe.dart';
import 'package:foodrecipes/views/widget/card_recipe.dart';

class ListPage extends StatefulWidget {
  const ListPage({Key? key}) : super(key: key);

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  late List<Recipe> _recipe;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    getRecipe();
  }

  Future<void> getRecipe() async {
    _recipe = await RecipeApi.getRecipe();
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(Icons.restaurant_menu),
            SizedBox(width: 15),
            Text(
              'Foods Recipes',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
          ],
        ),
      ),
      // ignore: prefer_const_constructors
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: _recipe.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: CardRecipe(
                      title: _recipe[index].name,
                      cookTime: _recipe[index].totalTime,
                      rating: _recipe[index].rating.toString(),
                      thumbnailUrl: _recipe[index].images),
                  onTap: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailRecipe(
                            name: _recipe[index].name,
                            totalTime: _recipe[index].totalTime,
                            rating: _recipe[index].rating.toString(),
                            images: _recipe[index].images,
                            description: _recipe[index].description,
                          ),
                        ))
                  },
                );
              },
            ),
    );
  }
}
