import 'package:flutter/material.dart';
import 'package:foodrecipes/views/bottom_nav.dart';
import 'package:foodrecipes/views/list.dart';
import 'package:foodrecipes/views/login.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:foodrecipes/views/home.dart';
import 'package:foodrecipes/views/splashscreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Food Recipes',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Color.fromARGB(255, 0, 0, 0),
          textTheme: TextTheme(bodyText2: TextStyle(color: Colors.white))),
      home: SplashScreen(),
    );
  }
}
