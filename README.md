# foodrecipes

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Berikut API yang saya gunakan dalam penbuatan foodrecipes App:
https://rapidapi.com/apidojo/api/tasty/

Tampilan Halaman awal yaitu tampikan login dan register.
user dapat melakukan pendaftaran dengan email dan password lalu klik register, dan jika sudah memiliki akun
bisa meng ngeklik login.

- Auth sudah menggunakan firebase

halaman home berisi halaman welcome

untuk menu list saya tampilkan beberapa list resep makanan dan bisa dilihat detail makanan juga.

pada halaman profil berisi halaman sederhana yaitu tentang bio saya sendiri

Link Figma
https://www.figma.com/file/0ocOGudCr27E0NgpIwlARJ/Final-Project-(Copy)?node-id=1%3A3
